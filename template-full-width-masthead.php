<?php
/**
 * Template Name: Full Width w/ Masthead
 */
?>
<?php while (have_posts()) : the_post(); ?>
  <?php the_content(); ?>
<?php endwhile; ?>