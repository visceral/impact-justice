<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
use Roots\Sage\Extras;

?>

<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
  <a id="skip-to-content" href="#main-content" class="no-print" aria-label="<?php _e('Skip to content'); ?>" title="<?php _e('Skip to content'); ?>"><?php _e('Skip to content'); ?></a>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'visceral'); ?>
      </div>
    <![endif]-->
    <?php
    /**
    * Header
    */
    do_action(' get_header');
    get_template_part('templates/header');
    ?>


    <?php
    /**
    * Masthead
    */
    if ( Extras\has_masthead() ) {
      get_template_part('templates/header-masthead');
    }

    $wrapper_class = (!is_page_template('template-full-width.php') && !is_page_template('template-full-width-masthead.php')) ? 'wrap container' : 'wrap full-width';
	  ?>
    
    <div class="<?php echo $wrapper_class; ?>" role="document">
      <div class="content">
        <main id="main-content" class="main" tabindex="-1">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
