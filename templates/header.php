<header class="banner container-fluid">
	<div id="header-main">
		<a class="brand" href="<?= esc_url(home_url('/')); ?>">
			<?php bloginfo('name'); ?>
		</a>
		<nav class="nav-primary">
			<?php
			if (has_nav_menu('primary_menu')) :
				wp_nav_menu(['theme_location' => 'primary_menu', 'menu_class' => 'nav list-inline']);
			endif;
			?>
		</nav>
		<label id="mobile-nav-icon" for="nav-toggle" class="icon-menu no-print" tabindex="0"></label>
		<a id="search-button" class="icon-search" href="<?php echo site_url(); ?>/?s=" title="<?php _e('Search', 'sage');?>" aria-label="Search"><?php _e('Search', 'visceral'); ?></a>
	</div>
	<div id="header-search">
		<div class="flex-wrapper">
			<form role="search" class="searchform" method="get" action="<?php echo site_url() ?>">
				<label><span class="screen-reader-text"><?php _e('Search', 'sage'); ?></span>
					<input type="text" placeholder="<?php _e('Type here and hit enter', 'sage'); ?>..." name="s" autocomplete="off" spellcheck="false" autofocus>
				</label>
				<input type="submit" class="screen-reader-text" value="<?php _e('Submit', 'sage'); ?>">
			</form>
			<i id="search-close" class="icon-cancel" aria-label="Close"><?php _e('Close', 'visceral'); ?></i>
		</div>
	</div>
</header>
<input type="checkbox" id="nav-toggle">
<?php // Responsive-nav ?>
<?php if (has_nav_menu('mobile_menu')) :
	wp_nav_menu(['theme_location' => 'mobile_menu', 'container_id' => 'mobile-nav']);
elseif (has_nav_menu('primary_menu')) :
	wp_nav_menu(['theme_location' => 'primary_menu', 'container_id' => 'mobile-nav']);
endif;
?>
