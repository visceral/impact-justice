<?php
/**
 * Example list item templates
 *
 * This template is called from withing the page-example-list.php template.
 */
?>

<a href="<?php the_permalink(); ?>" <?php post_class('list-item'); ?>>
	<article>
		<?php
		/**
		* Example of .img-cover class
		*/
		?>
		<?php if ( has_post_thumbnail() ) : ?>
			<div class="img-cover">
				<?php
				$image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
				$w     = $image[1];
				$h     = $image[2];
				$class = ( $w < $h ) ? 'portrait' : '';
				the_post_thumbnail('medium', array('class' => $class) );
				?>
			</div>
		<?php endif; ?>

		<?php the_title(); ?>

		<?php
		/**
		* Example of getting post meta safely
		*/
		$job_title = get_post_meta(get_the_id(), 'job_title', true);
		if ( $job_title ) : ?>
			<p><?php echo $job_title; ?><p/>
		<?php endif; ?>
	</article>
</a>
