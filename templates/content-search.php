<?php $post_type_name = get_post_type_object( get_post_type() )->labels->singular_name; ?>
<article <?php post_class('search-result'); ?>>
	<div class="row md-reverse">
		<div class="column md-33 text-center">
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('medium'); ?></a>
		</div>
		<div class="column md-67">
			<header>
				<h3 class="entry-title <?php echo get_post_type(); ?>"><a href="<?php the_permalink(); ?>"><?php echo $post_type_name . ': ' . get_the_title(); ?></a></h3>
			</header>
			<div class="entry-summary">
				<p><?php the_excerpt(); ?></p>
			</div>
		</div>
	</div>
</article>
