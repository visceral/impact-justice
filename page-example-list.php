<?php
/**
 * Example listing page template
 *
 * This template assumes you have a page called 'example list' and a post type called 'example'
 * This template sets up a list (or grid) of posts and calls a template for each one.
 */

while (have_posts()) : the_post(); ?>

 	<?php the_content(); ?>

	<?php // Now let's set up a list of 'example' posts
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	$args = array(
		'post_type'              => 'example',
		'posts_per_page'         => 5,
		'paged'                  => $paged,
		'update_post_term_cache' => false, // Improves Query performance
		'update_post_meta_cache' => false, // Improves Query performance
	);
	$query = new WP_Query($args); ?>

	<?php if ( $query->have_posts() ) : ?>

		<div class="list-example">
			<?php while ($query->have_posts()) : $query->the_post(); ?>
				<?php get_template_part('templates/list-item', get_post_type()); ?>
			<?php endwhile; ?>
		</div>

		<?php // Pagination
		$total = $query->max_num_pages;
		// only bother with pagination if we have more than 1 page
		if ( $total > 1 ) : ?>
			<nav class="pagination text-center">
				<?php
				// Set up pagination to use later on
				$big = 999999999; // need an unlikely integer
				$pagination = paginate_links( array(
					'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format'    => '?paged=%#%',
					'current'   => max( 1, get_query_var('paged') ),
					'total'     => $total,
					'type'      => 'plain',
					'prev_next' => true,
					'prev_text' => __('Next', 'visceral'),
					'next_text' => __('Previous', 'visceral')
				) );

				echo $pagination; ?>
			</nav>
		<?php endif; ?>

	<?php else : ?>
		<?php // No posts :( ?>
	<?php endif; ?>

	<?php wp_reset_postdata(); // Don't forget this ?>

<?php endwhile; ?>
